# Libreplanet 2021

## A promotional poster / flyer for the Libreplanet 2021 conference.

- `logo.svg`: The libreplanet 2021 logo
- 'lp20201.odg`: the original source file
- `lp20201.pdf`: PDF version
- `lp20201.png`: PNG version
- `LICENSE`: a copy of the CC BY-SA 4.0 license
- `README.md`: this file

I created this to help promote the conference.  Please feel free to
change and modifiy to suit and share accordingly.

The materials in this repository are licensed under the terms of the
Creative Commons Attribution-ShareAlike 4.0 International Public
License Creative Commons Attribution-ShareAlike 4.0 International
Public License, a copy of which is available in the `LICENSE` file.
